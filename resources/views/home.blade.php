@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                @if(session()->has('alert'))
                    <div class="alert alert-{{ session()->get('alert.type') }}" role="alert">
                        {{ session()->get('alert.message') }}
                    </div>
                @endif
                <div class="card">
                    <div class="card-header">Send Notification</div>
                    <div class="card-body">
                        <form method="post" action="{{ route('notification.store') }}">
                            @csrf
                            <div class="form-group">
                                <label for="receiver_id">Select Receiver User</label>
                                <select class="form-control" id="receiver_id" name="receiver_id">
                                    <option selected>Choose...</option>
                                    @foreach(\App\User::where('id', '!=', auth()->user()->id)->get() as $user)
                                        <option value="{{ $user->id }}">{{ $user->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="title">Title</label>
                                <input type="text" class="form-control" id="title" name="title"
                                       placeholder="Notification Title"
                                       value="{{ old('title') }}">
                            </div>
                            <div class="form-group">
                                <label for="body">Body</label>
                                <textarea class="form-control" id="body" name="body"
                                          rows="3">{{ old('body') }}</textarea>
                            </div>
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection