<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::resource('notification', 'NotificationController');

Route::get('notifications', function () {
    $user = \auth()->user();
    $data['notifications'] = $user->notifications;
    $data['unseen_notifications_count'] = $user->notifications()->where('status', 'unseen')->count();
    return $data;
});
